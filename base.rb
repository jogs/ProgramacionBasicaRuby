=begin
	Este es el archivo base para trabajar
	sustituir la palabra 'Base' por el nombre de la clase
	
	Nota se recomienda que la clase tenga el mismo nombre
	que el archivo, y usar CamellCase
	
	Y la palabra 'Ruby' por el nombre del objeto a instanciar
=end
#se define la clase		
class Base
	def initialize()
	end
	def ejecutar()
		
	end
end

#se define un objeto de la clase
Ruby = Base.new()
Ruby.ejecutar
gets()