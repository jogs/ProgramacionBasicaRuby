=begin
	Autor: Joaquín García Santiago
	---------------------------------------------------
	Primer automata finito
	El Automata valida la siguienete expresion Regular
	"(0+1)*01"
	para un lenguaje binario de {0,1}	
=end
#Creando la clase AutomataFinito		
class AutomataFinito
#		@expresion = '0+1'
#		@tamanio = 0
	
		def q0(indice)
			i = indice
			puts "indice: #{i} , en q0"
			if i <= @tamanio
				if @expresion[i] == '0'
					q1(i+1)
				elsif @expresion[i] == '1'
					q0(i+1)
				else
					error
				end
			else
				error
			end
		end
		
		def q1(indice)
			i = indice
			puts "indice: #{i} , en q1"
			if i <= @tamanio
				if @expresion[i] == '0'
					q1(i+1)
				elsif @expresion[i] == '1'
					q2(i+1)
				else
					error
				end
			else
				error
			end
		end
		
		def q2(indice)
			i = indice
			puts "indice: #{i} , en q2"
			
			
			if i >= @tamanio
				puts "aqui termina"
				if @expresion[i-1] == '0'
					puts 'Validacion completa, la cadena no coinside'
				elsif @expresion[i-1] == '1'
					puts 'Validacion completa, la cadena coinside'
				else
					error
				end
			elsif i < @tamanio 
				if @expresion[i] == '0'
					q1(i+1)
				elsif @expresion[i] == '1'
					q0(i+1)
				else
					error
				end
			else
				error
			end
		end
		
		def error()
			puts 'El programa a terminado con un error'
		#	puts "El error puede estar en el caracter numero #{i}"
		end

	def initialize()
		puts 'Bienvenido, Ingrese una expresion regular'
		puts 'que coinsida con "(0+1)*01"'
		@expresion = gets.chomp
		@tamanio = @expresion.length
		puts "Se validara la siguiente expresion: #{@expresion}, de longitud: #{@tamanio}"
		i = 0
		@tamanio = @tamanio - 1
		q0(i)
	end
end

#se define un objeto de la clase
Automata = AutomataFinito.new
gets()