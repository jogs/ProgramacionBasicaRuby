=begin
	Para pasar de un tipo de dato a otro se usa:
	to_i	para pasar a integer
	to_f	para pasar a float
	to_s	para pasar a string					
=end
#se define la clase
$variable = "soy una variable global"		
class Base
	def initialize()
		@variable = "soy una variable de instancia"
	end
	def ejecutar()
		#una variable se define de manera automatica al llamarla
		valorUno = 5 	#Se definio como entero
		valorDos = 3.1	#Se definio como flotante
		cadena = "abc"	#Se definio como string
		variable = "soy una variable local"

		puts variable
		puts $variable
		puts @variable
	end
end
=begin
	una variable global puede ser llamada desde 
	cualquier clase del proyecto
	
	una variable de instancia puede ser llamada 
	desde cualquier parte de la clase donde se declaro

	una variable local solo puede ser usada por
	el metodo donde se definio					
=end

#se define un objeto de la clase
Ruby = Base.new()
Ruby.ejecutar
gets()