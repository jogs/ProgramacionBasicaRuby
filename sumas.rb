#se define la clase		
class Base
	def initialize()
	end
	# en ruby no existen los tipos de dato,
	# por lo que uno suma (o concatena) objetos
	def suma(a, b)
		a + b
	end
end

#se define un objeto de la clase
Ruby = Base.new()
puts Ruby.suma("hola ", "mundo!")
puts Ruby.suma(10, 8)
gets()