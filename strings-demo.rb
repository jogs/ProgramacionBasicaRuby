#se define la clase		
class Base
	def initialize()
	end
	def ejecutar()
		# Declaración de una cadena
		puts a = "ybur ne odnamargorp"
		# Concatenación de operaciones sobre la cadena
		puts a.reverse.capitalize.gsub(/l/, '*')
		# Extracción de los 4 primeros caracteres
		puts a.reverse[0..4]
		# Comparacion de cadenas
		puts a = "HOLA"
		puts b = "hola"
		puts a == b.swapcase
		# ¿Por que no orientarlo todo a objetos?
		puts a.eql?(b.swapcase)
		puts a == b
	end
end

#se define un objeto de la clase
Ruby = Base.new()
Ruby.ejecutar
gets()