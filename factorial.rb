#se define la clase		
class Base
	def initialize()
	end
	def factorial(n)
		if n == 0
			1
		else
			n * factorial(n-1)
		end
	end
end

#se define un objeto de la clase
Ruby = Base.new()
print Ruby.factorial(10)
gets()