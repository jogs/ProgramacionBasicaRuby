#se define la clase		
class Base
	def initialize()
	end
	def ejecutar()
		#puts realiza una impresion con un salto de linea y retorno de carro al final
		puts "Este es un mensaje con salto de linea"
		#print imprime solo la cadena y nada mas
		print "Este es un mensaje "
		print "sin salto de linea"
		#las comillas dobles interpretan el contenido
		puts "\t****\n****"
		#las comillas simples imprimen tal cual el contenido
		puts '\t****\n****'
	end
end

#se define un objeto de la clase HolaMundo
Ruby = Base.new()
Ruby.ejecutar
gets()